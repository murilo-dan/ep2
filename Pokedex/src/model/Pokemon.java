/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author murilo
 */
public class Pokemon {

    private String name;
    private String url_pokemon;

    Pokemon(String name, String url_pokemon) {
        this.name = name;
        this.url_pokemon = url_pokemon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl_pokemon() {
        return url_pokemon;
    }

    public void setUrl_pokemon(String url_pokemon) {
        this.url_pokemon = url_pokemon;
    }

}
